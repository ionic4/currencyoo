export class Rate {
    currency: string;
    exchangeRate: number;

    constructor(currency: string, exchangeRate: number) {
        this.currency = currency;
        this.exchangeRate = exchangeRate;
    }
}
