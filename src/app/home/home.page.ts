import { Component, OnInit } from '@angular/core';
import { CurrencyService } from '../currency.service';
import { DecimalPipe } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  items: any;
  amount: number;
  result: number;
  currency: string;

  constructor(private currencyService: CurrencyService,private  router: Router) {}

  ngOnInit() {
    this.amount = 0;
    this.items = this.currencyService.getRates();
    this.result = 0;
  }

  convert() {
    this.currencyService.setToCurrency(this.currency);
    this.result = this.currencyService.convert(this.amount);
  }

  show_rates() {
    this.router.navigateByUrl('rates');
  }

}
